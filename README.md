![Build Status](https://marcos.amorim.gitlab.io/design-system-movida/imagens/logo-movida-design-system.svg)


---

##Introdução

MOVIDA Design System é o sistema de design do grupo MOVIDA para sistemas e produtos que demandam experiências digitais. 

Com o MOVIDA Design System como guia, o intuito é fornecer parâmetros e subsídios de trabalho como ferramentas e recursos de design, diretrizes de interface de usuário constantemente 
em evolução e desenvolvimento através dos colaboradores do comitê de UX da MOVIDA.

Como sistema de design oficial da MOVIDA, o MOVIDA Design System atende a uma ampla gama de designers e desenvolvedores que criam produtos e experiências digitais. Os objetivos
do sistema incluem melhorar a consistência e a qualidade da experiência/ UI, tornando o processo de design e desenvolvimento mais eficiente, estabelecendo um vocabulário 
compartilhado entre designer e desenvolvedor e fornecendo orientação clara e identificável sobre as melhores práticas de UX/ UI.


##Princípios

**Constante evolução**

O MOVIDA Design System é o sistema que está em constante evolução e melhorias progressivas serão sempre aplicadas. 

---

**Inclusivo**

Projetado e construído para ser acessível a todos, independentemente da capacidade ou situação.

---

**Modular e Flexível**

O MOVIDA Design System é modular e flexível. A modularidade garante máxima flexibilidade na execução. Seus componentes são projetados para funcionar perfeitamente entre si em qualquer combinação que atenda às necessidades do usuário.

---

**Usuário em primeiro lugar**

MOVIDA Design System coloca o usuário em primeiro lugar. Começando com pesquisas rigorosas sobre as necessidades e desejos dos usuários e revisado continuamente com base em seus comentários.

---

**Construindo consistência**

MOVIDA Design System constrói consistência. Todos os elementos e componentes foram projetados desde o início para funcionarem juntos para garantir experiências de usuário consistentes e coesas.

---

###Comitê UX Movida

> [Link do grupo no Slack](https://join.slack.com/t/ux-movida/shared_invite/enQtNjM4MjIzNDE5NTIzLTc0ZjFhZDdkODE3MWRhN2I4ZWZiNGM2Y2UzYWI2MzE3Zjk2ZTQxNmExMzU1ZmI3ZDNjZWY3YzA5OTA2YjQwYmE)
