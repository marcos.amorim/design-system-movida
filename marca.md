#A Marca


---

##Logotipo corporativo

O logotipo corporativo Movida sintetiza a personalidade e a projeção do grupo, identicando-o nas suas
comunicações no plano institucional e mercadológico e tornando-o reconhecível aos seus diversos públicos.
O logotipo corporativo foi desenvolvido dentro de padrões universalmente aceitos, no intuito de assegurar
a devida projeção e facilitar a sua utilização. É nosso dever seguir as normas aqui traçadas para que o logotipo
corporativo seja aplicado de maneira uniforme em todas as manifestações da empresa. Em caso de dúvidas,
esclarecimentos ou informações adicionais, deverá ser consultado o Departamento de Assuntos Corporativos.
Os casos excepcionais que por ventura possam surgir serão denidos pelo Departamento de Assuntos Corporativos
em conjunto com as áreas envolvidas.
Para desenvolvermos a construção do logotipo e possibilitar a sua reprodução mecânica, utilizamos unidades de
medida, denominadas X e Y, equivalentes a 5 mm. Toda a construção do logotipo e suas relações serão obtidas
a partir dessas unidades, como mostrado no grid abaixo.

![Logotipo grade](https://gitlab.com/marcos.amorim/design-system-movida/raw/master/imagens/logotipo-grade.JPG)





##Princípios

**Constante evolução**

O MOVIDA Design System é o sistema que está em constante evolução e melhorias progressivas serão sempre aplicadas. 

---

**Inclusivo**

Projetado e construído para ser acessível a todos, independentemente da capacidade ou situação.

---

**Modular e Flexível**

O MOVIDA Design System é modular e flexível. A modularidade garante máxima flexibilidade na execução. Seus componentes são projetados para funcionar perfeitamente entre si em qualquer combinação que atenda às necessidades do usuário.

---

**Usuário em primeiro lugar**

MOVIDA Design System coloca o usuário em primeiro lugar. Começando com pesquisas rigorosas sobre as necessidades e desejos dos usuários e revisado continuamente com base em seus comentários.

---

**Construindo consistência**

MOVIDA Design System constrói consistência. Todos os elementos e componentes foram projetados desde o início para funcionarem juntos para garantir experiências de usuário consistentes e coesas.

---

###Comitê UX Movida

> [Link do grupo no Slack](https://join.slack.com/t/ux-movida/shared_invite/enQtNjM4MjIzNDE5NTIzLTc0ZjFhZDdkODE3MWRhN2I4ZWZiNGM2Y2UzYWI2MzE3Zjk2ZTQxNmExMzU1ZmI3ZDNjZWY3YzA5OTA2YjQwYmE)



















```yaml
# Link do Comitê no Slack


```


Example [GitBook] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# requiring the environment of NodeJS 8.9.x LTS (carbon)
image: node:8.9

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch latest # fetch latest stable version
  - gitbook install # add any requested plugins in book.json
  #- gitbook fetch pre # fetch latest pre-release version
  #- gitbook fetch 2.6.7 # fetch specific version

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
  only:
    - master # this job will affect only the 'master' branch
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] GitBook `npm install gitbook-cli -g`
1. Fetch GitBook's latest stable version `gitbook fetch latest`
1. Preview your project: `gitbook serve`
1. Add content
1. Generate the website: `gitbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at GitBook's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
